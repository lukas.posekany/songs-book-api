FROM golang:alpine

ADD ./src /go/src/app

WORKDIR /go/src/app

ENV GOPATH=/go/src/app

ENTRYPOINT ["go", "run", "songsBook.go"]