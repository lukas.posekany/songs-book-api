[1%Well you done done me and you bet I felt it
I tried to be chill but you're so hot I melted
I fell right through the cracks
And now I'm trying to get back
Before the cool done run out
I'll be giving it my best-est
And nothing's going to stop me but divine intervention
I reckon it's again my turn,
To win some or learn some]

[R1%But I won't hesitate no more, no more
It cannot wait, I'm yours]

[2%Well open up your mind and see like me
Open up your plans and damn you're free
Look into your heart and you'll find love love love love
Listen to the music of the moment people dance and sing
We are just one big family
And it's our God-forsaken right to be loved loved loved loved loved]

[R2%So I won't hesitate no more, no more
It cannot wait I'm sure
There's no need to complicate
Our time is short
This is our fate, I'm yours]

[3%I've been spending way too long checking my tongue in the mirror
And bending over backwards just to try to see it clearer
But my breath fogged up the glass
And so I drew a new face and I laughed
I guess what I been saying is there ain't no better reason
To rid yourself of vanity and just go with the seasons
It's what we aim to do our name is our virtue]

[R1%]
