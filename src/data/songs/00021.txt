[1%*Em*Můj doktor sebou *D*sek
a *A*málem to neu*Em*stál.
*Em*Víš, já mu tehdy *D*řek,
že *A*jsem gastrosexu*Em*ál]

[2%Ale *Em*jen co se z lehu *D*zved,
*A*přísně se zazu*Em*bil.
*Em*Blíž jsem si k němu *D*sed
a *A*všechno mu vyklo*C*pil.]

[R%Do *Am*příboru se oša*D*tím a nebude mi *Em*zima,*C*
*Am*stříknu pivo na ru*D*kávy a kolu do ga*Em*tí, *D*
*Am*vzít si boty z kavi*D*áru není volov*Em*ina*C*,
na *Am*léto si pak uva*D*řím, šaty kopro*Em*vý]

[3%Doktor řek no pane Homola,
tím ste mě zaskočil.
Přemýšlím stále dokola,
čím bych vás vyléčil.]

[4%S tou tváří vaší nevinnou,
Já bych nemarodil.
Zkuste svůj úlet rozvinout,
doktor mi poradil.]

[R%]

[R+%a gu*D*lášo*A*vý fi*Em*ží,
noha*Em*vičky z marci*D*pánu s vani*A*lkovou příchu*Em*tí.
*Em*Šál mazaný*D* bruše*A*tou z tymi*Em*ánu,
a knof*Em*líky ze sa*D*lámu, kula*A*ťoučký vykro*C*jím.]

[R%]
